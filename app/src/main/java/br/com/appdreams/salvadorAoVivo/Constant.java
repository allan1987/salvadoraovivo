package br.com.appdreams.salvadorAoVivo;

public class Constant {

    public static final String APIARY_URL = "http://private-6d2cd-salvadoraovivo.apiary-mock.com";
    public static final String IP_WEBCAM_URL = "rtsp://192.168.2.148:1234";

    public static final float MAX_HEIGHT_PIXELS = 83;
    public static final int MAX_WIDTH = 400;

    public static final String STREAM = "stream";
}
