package br.com.appdreams.salvadorAoVivo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties({""})
public class Stream implements Serializable {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("image_url")
    private String imageUrl;

    @JsonProperty("stream_url")
    private String streamUrl;

    public Stream() {}

    public Stream(Long id, String title, String imageUrl, String streamUrl) {
        this.id = id;
        this.title = title;
        this.imageUrl = imageUrl;
        this.streamUrl = streamUrl;
    }

    public Long getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public String getStreamUrl() {
        return this.streamUrl;
    }
}