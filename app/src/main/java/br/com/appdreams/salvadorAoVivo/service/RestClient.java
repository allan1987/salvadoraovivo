package br.com.appdreams.salvadorAoVivo.service;


import br.com.appdreams.salvadorAoVivo.model.StreamsContent;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Streaming;

public interface RestClient {

    @GET("/streams")
    public void getStreams(Callback<StreamsContent> callbackHandler);

    @GET("/shot.jpg")
    @Streaming
    public Response getVideoStream();
}
