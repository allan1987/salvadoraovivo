package br.com.appdreams.salvadorAoVivo.view;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import br.com.appdreams.salvadorAoVivo.Constant;
import br.com.appdreams.salvadorAoVivo.R;
import br.com.appdreams.salvadorAoVivo.controller.activity.StreamActivity_;
import br.com.appdreams.salvadorAoVivo.controller.event.request.RequestStartActivityEvent;
import br.com.appdreams.salvadorAoVivo.model.Stream;
import de.greenrobot.event.EventBus;

@EViewGroup(R.layout.stream_item)
public class StreamItemView extends CardView implements View.OnClickListener {

    @ViewById
    public ImageView contentImageView;

    @ViewById
    public TextView contentTitleTxt;

    private Stream stream;

    public StreamItemView(Context context) {
        super(context);
        this.setOnClickListener(this);
    }

    public void bind(final Stream stream) {
        this.stream = stream;

        //Gets height dip
        int heightDIP = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                Constant.MAX_HEIGHT_PIXELS, this.getResources().getDisplayMetrics());

        DrawableTypeRequest<String> dRequest = Glide.with(this.getContext()).load(stream.getImageUrl());

        dRequest.placeholder(R.drawable.img_placeholder)
                .override(Constant.MAX_WIDTH, heightDIP)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .into(this.contentImageView);

        this.contentTitleTxt.setText(stream.getTitle());
    }

    @Override
    public void onClick(View view) {
        Map<String, Serializable> map = new HashMap<>();
        map.put(Constant.STREAM, this.stream);

        EventBus.getDefault().post(new RequestStartActivityEvent(getContext(), StreamActivity_.class, map));
    }
}