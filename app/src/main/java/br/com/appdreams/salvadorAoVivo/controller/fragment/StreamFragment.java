package br.com.appdreams.salvadorAoVivo.controller.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EFragment;

import br.com.appdreams.salvadorAoVivo.Constant;
import br.com.appdreams.salvadorAoVivo.R;
import br.com.appdreams.salvadorAoVivo.model.Stream;
import io.vov.vitamio.LibsChecker;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.widget.VideoView;

@EFragment
public class StreamFragment extends Fragment implements View.OnClickListener {

    private static final int bufferSize = 1024, videoQuality = 16;

    private boolean handling = false, alreadyStarted = false;

    private Handler handler = new Handler();

    private RelativeLayout infoBarLayout;

    private VideoView streamVideo;

    private TextView bufferTxt, contentTitleTxt;

    private Stream stream;

    private Display display;

    public StreamFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        WindowManager wm = (WindowManager) this.getActivity().getSystemService(Context.WINDOW_SERVICE);

        this.display = wm.getDefaultDisplay();

        View view = inflater.inflate(R.layout.fragment_stream, null);

        this.stream = (Stream)this.getArguments().getSerializable(Constant.STREAM);

        this.infoBarLayout = (RelativeLayout)view.findViewById(R.id.infoBarLayout);

        this.contentTitleTxt = (TextView)view.findViewById(R.id.contentTitleTxt);
        this.contentTitleTxt.setText(this.stream.getTitle());

        this.bufferTxt = (TextView)view.findViewById(R.id.bufferTxt);

        this.streamVideo = (VideoView)view.findViewById(R.id.streamVideo);

        view.setOnClickListener(this);

        return view;
    }

    public void start() {
        if (!LibsChecker.checkVitamioLibs(this.getActivity())) {
            return;
        }

        this.changeContentTitleVisibility();

        // you may need to add " live=1" at the end of uri if it is live in red5 server
        Uri streamUri = Uri.parse(this.stream.getStreamUrl());

        Log.e("ABC", "streamUri = " + streamUri.toString());

        this.streamVideo.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            private boolean needResume;

            @Override
            public boolean onInfo(MediaPlayer mediaPlayer, int mediaInfoID, int rate) {
                switch (mediaInfoID) {
                    case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                        Log.e("ABC","MediaPlayer.MEDIA_INFO_BUFFERING_START");
                        //Begin buffer, pause playing
                        if(streamVideo.isPlaying()) {
                            streamVideo.stopPlayback();
                        }
                        if(alreadyStarted) {
                            alreadyStarted = false;
                            start();
                        }

                        needResume = true;

                        bufferTxt.setText("Carregando...");
                        bufferTxt.setVisibility(View.VISIBLE);
                        break;
                    case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                        Log.e("ABC","MediaPlayer.MEDIA_INFO_BUFFERING_END");
                        //The buffering is done, resume playing
                        if(needResume) {
                            streamVideo.start();
                            needResume = false;
                        }
                        alreadyStarted = true;
                        bufferTxt.setVisibility(View.GONE);
                        break;
                    case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
//                        Log.e("ABC","MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED");
                        //Display video download speed
//                        Log.e("ABC","download rate: " + rate);
                        break;
                }
                return true;
            }
        });

        this.streamVideo.setVideoURI(streamUri);

//        this.streamVideo.setMediaController(new MediaController(this.getActivity()));
        this.streamVideo.requestFocus();

        this.streamVideo.setBufferSize(bufferSize);
        this.streamVideo.setVideoQuality(videoQuality);

        this.streamVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                Log.e("ABC","OnPrepared");
                mediaPlayer.setPlaybackSpeed(1.0f);
            }
        });

        this.streamVideo.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int i) {
                bufferTxt.setText("Carregando... " + i + "%");
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Point size = new Point();
        this.display.getSize(size);

        int width = size.x;
        int height = (int)(width / this.streamVideo.getVideoAspectRatio());

        RelativeLayout.LayoutParams videoviewlp = new RelativeLayout.LayoutParams(width, height);

        videoviewlp.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        videoviewlp.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);

        this.streamVideo.setLayoutParams(videoviewlp);
        this.streamVideo.invalidate();
    }

    @Override
    public void onClick(View view) {
        this.changeContentTitleVisibility();
    }

    private void changeContentTitleVisibility() {
        if(this.infoBarLayout.getVisibility() == View.GONE) {
            this.infoBarLayout.setVisibility(View.VISIBLE);
            this.handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(handling && (infoBarLayout.getVisibility() == View.VISIBLE)) {
                        infoBarLayout.setVisibility(View.GONE);
                        handling = false;
                    }
                }
            }, 3000);
            handling = true;
        }
        else if(this.infoBarLayout.getVisibility() == View.VISIBLE) {
            this.infoBarLayout.setVisibility(View.GONE);
            handling = false;
        }
    }

    @Override
    public void onStart() {
        this.start();
        Log.e("ABC","onStart");
        super.onStart();
    }

    @Override
    public void onStop() {
        Log.e("ABC","onStop");
        this.streamVideo.stopPlayback();
        super.onStop();
    }
}