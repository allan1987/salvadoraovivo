package br.com.appdreams.salvadorAoVivo.controller.fragment;

import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import br.com.appdreams.salvadorAoVivo.Constant;
import br.com.appdreams.salvadorAoVivo.R;

import br.com.appdreams.salvadorAoVivo.controller.activity.StreamActivity_;
import br.com.appdreams.salvadorAoVivo.controller.adapter.StreamAdapter;
import br.com.appdreams.salvadorAoVivo.controller.event.request.RequestStartActivityEvent;
import br.com.appdreams.salvadorAoVivo.controller.event.response.ResponseStreamsEvent;
import br.com.appdreams.salvadorAoVivo.model.Stream;
import de.greenrobot.event.EventBus;

@EFragment(R.layout.fragment_main)
public class MainFragment extends Fragment {

    private boolean loading;

    @ViewById
    public RecyclerView streamsRecyclerView;

    private StreamAdapter adapter;

    public MainFragment() {
        this.loading = true;
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @AfterViews
    public void afterViews() {
        this.streamsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.streamsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        this.adapter = new StreamAdapter(this.getActivity());
        this.streamsRecyclerView.setAdapter(this.adapter);

//        this.addOnScrollListener();
    }

//    private void addOnScrollListener() {
//        this.streamsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//
//            private int pastVisiblesItems
//                    ,
//                    visibleItemCount
//                    ,
//                    totalItemCount;
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                this.visibleItemCount = streamsRecyclerView.getLayoutManager().getChildCount();
//                this.totalItemCount = streamsRecyclerView.getLayoutManager().getItemCount();
//                this.pastVisiblesItems = ((LinearLayoutManager) streamsRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
//
//                if (loading) {
//                    if ((this.visibleItemCount + this.pastVisiblesItems) >= this.totalItemCount) {
//                        loading = false;
//                        EventBus.getDefault().post(new RequestShotsEvent(getActivity(), ++pageNumber));
//                    }
//                }
//            }
//        });
//    }

    public void onEvent(ResponseStreamsEvent event) {
        this.adapter.addAll(event.getStreamList());
        this.adapter.notifyDataSetChanged();
        this.loading = true;

        Stream stream = new Stream(10l, "Teste", "", "rtsp://192.168.2.148:1234/trackID=1;seq=0");
        Map<String, Serializable> map = new HashMap<>();
        map.put(Constant.STREAM, stream);

        EventBus.getDefault().post(new RequestStartActivityEvent(this.getActivity(), StreamActivity_.class, map));
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}