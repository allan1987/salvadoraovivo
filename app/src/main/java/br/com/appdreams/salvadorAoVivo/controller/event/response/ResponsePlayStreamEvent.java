package br.com.appdreams.salvadorAoVivo.controller.event.response;

import android.content.Context;

import br.com.appdreams.salvadorAoVivo.model.Stream;

public class ResponsePlayStreamEvent {

    private final Context context;
    private final Stream stream;

    public ResponsePlayStreamEvent(Context context, Stream stream) {
        this.context = context;
        this.stream = stream;
    }

    public Context getContext() {
        return this.context;
    }

    public Stream getStream() {
        return this.stream;
    }
}
