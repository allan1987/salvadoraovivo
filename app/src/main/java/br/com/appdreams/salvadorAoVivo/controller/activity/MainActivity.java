package br.com.appdreams.salvadorAoVivo.controller.activity;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsMenu;

import br.com.appdreams.salvadorAoVivo.R;
import br.com.appdreams.salvadorAoVivo.controller.event.EventManager;
import br.com.appdreams.salvadorAoVivo.controller.event.request.RequestStreamsEvent;
import br.com.appdreams.salvadorAoVivo.controller.fragment.MainFragment_;
import br.com.appdreams.salvadorAoVivo.util.DialogUtil;
import de.greenrobot.event.EventBus;

@EActivity(R.layout.activity_default)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;

    public EventManager eventManager = EventManager.getInstance();

    public MainFragment_ mainFragment;

    @AfterViews
    public void afterViews() {
        Glide.get(this).setMemoryCategory(MemoryCategory.HIGH);

        this.fragmentManager = this.getSupportFragmentManager();

        if(this.fragmentManager.findFragmentById(R.id.containerLayout) == null) {
            this.mainFragment = new MainFragment_();

            this.fragmentManager.beginTransaction()
                    .add(R.id.containerLayout, this.mainFragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_exit) {
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.eventManager.init();

        EventBus.getDefault().post(new RequestStreamsEvent(this));
    }

    @Override
    protected void onPause() {
        DialogUtil.getInstance(this).dismiss();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        this.eventManager.destroy();
        super.onDestroy();
    }
}