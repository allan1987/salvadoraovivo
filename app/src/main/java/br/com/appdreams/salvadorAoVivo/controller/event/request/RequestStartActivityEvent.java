package br.com.appdreams.salvadorAoVivo.controller.event.request;

import android.app.Activity;
import android.content.Context;

import java.io.Serializable;
import java.util.Map;

public class RequestStartActivityEvent extends GenericRequestEvent {

    private final Class<? extends Activity> classType;
    private final Map<String, Serializable> params;

    public RequestStartActivityEvent(Context context, Class<? extends Activity> classType, Map<String, Serializable> params) {
        super(context);
        this.classType = classType;
        this.params = params;
    }

    public Class<? extends Activity> getClassType() {
        return this.classType;
    }

    public Map<String, Serializable> getParams() {
        return this.params;
    }
}
