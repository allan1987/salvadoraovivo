package br.com.appdreams.salvadorAoVivo.controller.module;

import javax.inject.Singleton;

import br.com.appdreams.salvadorAoVivo.Constant;
import br.com.appdreams.salvadorAoVivo.service.RestClient;
import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.converter.JacksonConverter;

@Module
public class AppModule {

  @Provides
  @Singleton
  RestClient provideRestClient() {
    return new RestAdapter.Builder()
            .setConverter(new JacksonConverter())
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .setEndpoint(Constant.APIARY_URL)
            .build()
            .create(RestClient.class);
  }

}
