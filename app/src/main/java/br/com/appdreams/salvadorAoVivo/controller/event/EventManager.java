package br.com.appdreams.salvadorAoVivo.controller.event;

import android.content.Context;
import android.content.Intent;

import com.google.gson.internal.Streams;

import br.com.appdreams.salvadorAoVivo.R;
import br.com.appdreams.salvadorAoVivo.controller.component.AppComponent;
import br.com.appdreams.salvadorAoVivo.controller.component.DaggerAppComponent;
import br.com.appdreams.salvadorAoVivo.controller.event.request.RequestPlayStreamEvent;
import br.com.appdreams.salvadorAoVivo.controller.event.request.RequestStartActivityEvent;
import br.com.appdreams.salvadorAoVivo.controller.event.request.RequestStreamsEvent;
import br.com.appdreams.salvadorAoVivo.controller.event.response.ResponsePlayStreamEvent;
import br.com.appdreams.salvadorAoVivo.controller.event.response.ResponseStreamsEvent;
import br.com.appdreams.salvadorAoVivo.controller.module.AppModule;
import br.com.appdreams.salvadorAoVivo.model.Stream;
import br.com.appdreams.salvadorAoVivo.model.StreamsContent;
import br.com.appdreams.salvadorAoVivo.service.RestClient;
import br.com.appdreams.salvadorAoVivo.util.DialogUtil;
import br.com.appdreams.salvadorAoVivo.util.GenericUtil;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class EventManager {

    private static EventManager instance;

    private RestClient restClient;

    public Context context;

    private EventManager() {}

    public static EventManager getInstance() {
        if(instance == null) {
            instance = new EventManager();
        }
        return instance;
    }

    public void init() {
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        if(this.restClient == null) {
            AppComponent appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule())
                    .build();

            this.restClient = appComponent.provideRestClient();
        }
    }

    public void onEvent(final RequestStreamsEvent event) {
        this.context = event.getContext();

        if(!GenericUtil.isOnline(this.context)) {
            DialogUtil.getInstance(this.context).makeAlert(R.string.offline);
            return;
        }
        else {
            DialogUtil.getInstance(this.context).showWaiting(false);
        }

        this.restClient.getStreams(new Callback<StreamsContent>() {
            @Override
            public void success(StreamsContent streamContent, Response response) {
                EventBus.getDefault().post(new ResponseStreamsEvent(streamContent.getStreamList()));
                DialogUtil.getInstance(context).dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                DialogUtil.getInstance(context).makeAlert(R.string.errorWasOcurred);
            }
        });
    }

    public void onEvent(final RequestStartActivityEvent event) {
        this.context = event.getContext();

        DialogUtil.getInstance(this.context).showWaiting(false);

        Intent intent = new Intent(this.context, event.getClassType());
        for(String key : event.getParams().keySet()) {
            intent.putExtra(key, event.getParams().get(key));
        }
        this.context.startActivity(intent);
    }

    public void destroy() {
        EventBus.getDefault().unregister(this);
    }
}