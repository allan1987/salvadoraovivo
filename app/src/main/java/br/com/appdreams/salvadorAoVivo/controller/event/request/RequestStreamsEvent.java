package br.com.appdreams.salvadorAoVivo.controller.event.request;

import android.content.Context;

public class RequestStreamsEvent extends GenericRequestEvent {

    public RequestStreamsEvent(Context context) {
        super(context);
    }
}
