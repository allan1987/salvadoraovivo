package br.com.appdreams.salvadorAoVivo.controller.event.request;

import android.content.Context;

import br.com.appdreams.salvadorAoVivo.model.Stream;

public class RequestPlayStreamEvent extends GenericRequestEvent {

    private final Stream stream;

    public RequestPlayStreamEvent(Context context, Stream stream) {
        super(context);
        this.stream = stream;
    }

    public Stream getStream() {
        return this.stream;
    }
}
