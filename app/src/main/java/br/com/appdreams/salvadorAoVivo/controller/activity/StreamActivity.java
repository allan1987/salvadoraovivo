package br.com.appdreams.salvadorAoVivo.controller.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.Window;

import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import br.com.appdreams.salvadorAoVivo.Constant;
import br.com.appdreams.salvadorAoVivo.R;
import br.com.appdreams.salvadorAoVivo.controller.event.EventManager;
import br.com.appdreams.salvadorAoVivo.controller.event.request.RequestPlayStreamEvent;
import br.com.appdreams.salvadorAoVivo.controller.fragment.StreamFragment_;
import br.com.appdreams.salvadorAoVivo.model.Stream;
import de.greenrobot.event.EventBus;

@EActivity(R.layout.activity_default)
public class StreamActivity extends FragmentActivity {

    private FragmentManager fragmentManager;

    public StreamFragment_ streamFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @AfterViews
    public void afterViews() {

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constant.STREAM, this.getIntent().getSerializableExtra(Constant.STREAM));

        this.fragmentManager = this.getSupportFragmentManager();

        if(this.fragmentManager.findFragmentById(R.id.containerLayout) == null) {
            this.streamFragment = new StreamFragment_();
            this.streamFragment.setArguments(bundle);

            this.fragmentManager.beginTransaction()
                    .add(R.id.containerLayout, this.streamFragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}