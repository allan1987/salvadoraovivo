package br.com.appdreams.salvadorAoVivo.controller.activity;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.Window;

import br.com.appdreams.salvadorAoVivo.controller.camera.CameraPreview;
import br.com.appdreams.salvadorAoVivo.controller.camera.HttpCameraPreview;

public class CamAppActivity extends Activity {

    private int viewWidth;
    private int viewHeight;

    private SurfaceView cameraPreview;

    private static final boolean useHttpCamera = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.calculateDisplayDimensions();

        if (this.useHttpCamera) {
            this.cameraPreview = new HttpCameraPreview(this, this.viewWidth, this.viewHeight);
        }
        else {
            this.cameraPreview = new CameraPreview(this);
        }
        this.setContentView(this.cameraPreview);
    }

    private void calculateDisplayDimensions() {
        Point size = new Point();
        this.getWindowManager().getDefaultDisplay().getSize(size);

        this.viewWidth = size.x;
        this.viewHeight = size.y;
    }
}