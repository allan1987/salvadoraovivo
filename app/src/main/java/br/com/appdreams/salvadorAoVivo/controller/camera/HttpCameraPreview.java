package br.com.appdreams.salvadorAoVivo.controller.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.List;

public class HttpCameraPreview extends SurfaceView implements SurfaceHolder.Callback {

//    private static final String url = "http://10.0.2.2:8080";
    private static final String url = "http://192.168.2.148:8080/shot.jpg";

    private CanvasThread canvasThread;

    private SurfaceHolder holder;
    private HttpCamera camera;

    private int viewWidth;
    private int viewHeight;

    public HttpCameraPreview(Context context, int viewWidth, int viewHeight) {
        super(context);
        this.holder = this.getHolder();
        this.holder.addCallback(this);
        this.holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;

        this.canvasThread = new CanvasThread();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder2, int format, int w, int h) {
//        try {
//            Canvas c = this.holder.lockCanvas(null);
//            this.camera.captureAndDraw(c);
//
//            if (c != null) {
//                this.holder.unlockCanvasAndPost(c);
//            }
//        }
//        catch (Exception e) {
//            Log.e(getClass().getSimpleName(), "Error when surface changed", e);
//            this.camera = null;
//        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0) {
        try {
            this.camera = new HttpCamera(this.url, this.viewWidth, this.viewHeight, true);
            this.canvasThread.setRunning(true);
            this.canvasThread.start();
        }

        catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error while creating surface", e);
            this.camera = null;
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        boolean retry = true;

        this.camera = null;
        this.canvasThread.setRunning(false);

        while (retry) {
            try {
                this.canvasThread.join();
                retry = false;
            } catch (InterruptedException e) {}
        }
    }

    private class CanvasThread extends Thread {

        private boolean running;

        public void setRunning(boolean running){
            this.running = running;
        }

        public void run() {
            while (this.running) {
                Canvas c = null;

                try {
                    c = holder.lockCanvas(null);

                    synchronized (holder) {
                        camera.captureAndDraw(c);
                    }
                }
                catch (Exception e) {
                    Log.e(getClass().getSimpleName(), "Error while drawing canvas", e);
                }
                finally {
                    if (c != null) {
                        holder.unlockCanvasAndPost(c);
                    }
                }
            }
        }
    }
}