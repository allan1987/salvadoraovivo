package br.com.appdreams.salvadorAoVivo.controller.component;

import javax.inject.Singleton;

import br.com.appdreams.salvadorAoVivo.controller.module.AppModule;
import br.com.appdreams.salvadorAoVivo.service.RestClient;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
  public RestClient provideRestClient();
}
