package br.com.appdreams.salvadorAoVivo.controller.camera;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.List;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder holder;
    private Camera camera;

    public CameraPreview(Context context) {
        super(context);
        this.holder = this.getHolder();
        this.holder.addCallback(this);
        this.holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Camera.Parameters parameters = this.camera.getParameters();
        List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();

        // You need to choose the most appropriate previewSize for your app
        Camera.Size previewSize = previewSizes.get(0);

        parameters.setPreviewSize(previewSize.width, previewSize.height);
        this.camera.setParameters(parameters);
        this.camera.startPreview();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder1) {
        try {
            this.camera = Camera.open(1);
            this.camera.setPreviewDisplay(holder1);
        }
        catch (Exception e) {
            Log.e("Exception", "e=" + e);
            this.camera.release();
            this.camera = null;
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        this.camera.stopPreview();
        this.camera.release();
        this.camera = null;
    }
}