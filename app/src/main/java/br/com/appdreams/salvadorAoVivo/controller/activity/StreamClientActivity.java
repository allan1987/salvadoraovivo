package br.com.appdreams.salvadorAoVivo.controller.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.majorkernelpanic.streaming.Session;
import net.majorkernelpanic.streaming.SessionBuilder;
import net.majorkernelpanic.streaming.gl.SurfaceView;
import net.majorkernelpanic.streaming.rtsp.RtspClient;
import net.majorkernelpanic.streaming.video.VideoQuality;

import br.com.appdreams.salvadorAoVivo.R;
import br.com.appdreams.salvadorAoVivo.service.MyRtspClient;
import br.com.appdreams.salvadorAoVivo.util.GenericUtil;

public class StreamClientActivity extends Activity implements OnClickListener, MyRtspClient.Callback,
Session.Callback, SurfaceHolder.Callback {

	public final static String TAG = "StreamClientActivity";

	private Button mButtonVideo;
	private ImageButton mButtonStart;
	private ImageButton mButtonCamera;
	private FrameLayout mLayoutVideoSettings;
	private FrameLayout mLayoutServerSettings;
	private SurfaceView mSurfaceView;
	private TextView mTextBitrate;
	private EditText mEditTextURI;
	private ProgressBar mProgressBar;
	private Session mSession;
	private MyRtspClient mClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);

		mButtonVideo = (Button) findViewById(R.id.video);
		mButtonStart = (ImageButton) findViewById(R.id.start);
		mButtonCamera = (ImageButton) findViewById(R.id.camera);
		mSurfaceView = (SurfaceView) findViewById(R.id.surface);
		mEditTextURI = (EditText) findViewById(R.id.uri);
		mTextBitrate = (TextView) findViewById(R.id.bitrate);
		mLayoutVideoSettings = (FrameLayout) findViewById(R.id.video_layout);
		mLayoutServerSettings = (FrameLayout) findViewById(R.id.server_layout);
		mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
		
		mButtonStart.setOnClickListener(this);
		mButtonCamera.setOnClickListener(this);
		mButtonVideo.setOnClickListener(this);

		SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(StreamClientActivity.this);
		if (mPrefs.getString("uri", null) != null) mLayoutServerSettings.setVisibility(View.GONE);
		mEditTextURI.setText(mPrefs.getString("uri", getString(R.string.default_stream)));

        if(true) {
            String mediaURL = "rtsp://192.168.2.148:1234/trackID=1;seq=0";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mediaURL));
            startActivity(intent);
            return;
        }

		// Configures the SessionBuilder
		mSession = SessionBuilder.getInstance()
				.setContext(getApplicationContext())
//				.setAudioEncoder(SessionBuilder.AUDIO_AAC)
//				.setAudioQuality(new AudioQuality(8000,16000))
				.setVideoEncoder(SessionBuilder.VIDEO_H264)
				.setSurfaceView(mSurfaceView)
				.setPreviewOrientation(0)
				.setCallback(this)
				.build();

		// Configures the RTSP client
		mClient = new MyRtspClient(GenericUtil.getLocalIP(this));
		mClient.setSession(mSession);
		mClient.setCallback(this);

		// Use this to force streaming with the MediaRecorder API
		//mSession.getVideoTrack().setStreamingMethod(MediaStream.MODE_MEDIARECORDER_API);

		// Use this to stream over TCP, EXPERIMENTAL!
		mClient.setTransportMode(RtspClient.TRANSPORT_UDP);

		// Use this if you want the aspect ratio of the surface view to 
		// respect the aspect ratio of the camera preview
		//mSurfaceView.setAspectRatioMode(SurfaceView.ASPECT_RATIO_PREVIEW);

		mSurfaceView.getHolder().addCallback(this);

//		selectQuality();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.start:
			mLayoutServerSettings.setVisibility(View.GONE);
			toggleStream();
			break;
		case R.id.camera:
			mSession.switchCamera();
			break;
		case R.id.settings:
			if (mLayoutVideoSettings.getVisibility() == View.GONE &&
			mLayoutServerSettings.getVisibility() == View.GONE) {
				mLayoutServerSettings.setVisibility(View.VISIBLE);
			} else {
				mLayoutServerSettings.setVisibility(View.GONE);
				mLayoutVideoSettings.setVisibility(View.GONE);
			}
			break;
		case R.id.save:
			mLayoutServerSettings.setVisibility(View.GONE);
			break;
		}
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
		mClient.release();
		mSession.release();
		mSurfaceView.getHolder().removeCallback(this);
	}

	private void enableUI() {
		mButtonStart.setEnabled(true);
		mButtonCamera.setEnabled(true);
	}

	// Connects/disconnects to the RTSP server and starts/stops the stream
	public void toggleStream() {
		mProgressBar.setVisibility(View.VISIBLE);
		if (!mClient.isStreaming()) {
			String ip,port,path;

			// We parse the URI written in the Editext
//			Pattern uri = Pattern.compile("rtsp://(.+):(\\d*)/(.+)");
//			Matcher m = uri.matcher(mEditTextURI.getText()); m.find();
			ip = "192.168.2.148";
			port = "1234";
			path = "";

//			mClient.setCredentials(mEditTextUsername.getText().toString(), mEditTextPassword.getText().toString());
			mClient.setServerAddress(ip, Integer.parseInt(port));
			mClient.setStreamPath("");
			mClient.startStream();

		} else {
			// Stops the stream and disconnects from the RTSP server
			mClient.stopStream();
		}
	}

	private void logError(final String msg) {
		final String error = (msg == null) ? "Error unknown" : msg; 
		// Displays a popup to report the eror to the user
		AlertDialog.Builder builder = new AlertDialog.Builder(StreamClientActivity.this);
		builder.setMessage(msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	@Override
	public void onBitrateUpdate(long bitrate) {
		mTextBitrate.setText(""+bitrate/1000+" kbps");
	}

	@Override
	public void onSessionConfigured() {}

    @Override
    public void onPreviewStarted() {}

    @Override
	public void onSessionStarted() {
		enableUI();
		mButtonStart.setImageResource(R.mipmap.ic_switch_video_active);
		mProgressBar.setVisibility(View.GONE);
	}

	@Override
	public void onSessionStopped() {
		enableUI();
		mButtonStart.setImageResource(R.mipmap.ic_switch_video);
		mProgressBar.setVisibility(View.GONE);
	}

	@Override
	public void onSessionError(int reason, int streamType, Exception e) {
		mProgressBar.setVisibility(View.GONE);
		switch (reason) {
		case Session.ERROR_CAMERA_ALREADY_IN_USE:
			break;
		case Session.ERROR_INVALID_SURFACE:
			break;
		case Session.ERROR_STORAGE_NOT_READY:
			break;
		case Session.ERROR_CONFIGURATION_NOT_SUPPORTED:
			VideoQuality quality = mSession.getVideoTrack().getVideoQuality();
			logError("The following settings are not supported on this phone: "+
			quality.toString()+" "+
			"("+e.getMessage()+")");
			e.printStackTrace();
			return;
		case Session.ERROR_OTHER:
			break;
		}

		if (e != null) {
			logError(e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void onRtspUpdate(int message, Exception e) {
		switch (message) {
		case RtspClient.ERROR_CONNECTION_FAILED:
		case RtspClient.ERROR_WRONG_CREDENTIALS:
			mProgressBar.setVisibility(View.GONE);
			enableUI();
			logError(e.getMessage());
			e.printStackTrace();
			break;
		}
	}


	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		mSession.startPreview();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		mClient.stopStream();
	}
}
