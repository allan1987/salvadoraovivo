package br.com.appdreams.salvadorAoVivo.controller.event.response;

import java.util.List;

import br.com.appdreams.salvadorAoVivo.model.Stream;

public class ResponseStreamsEvent {

    private final List<Stream> streamList;

    public ResponseStreamsEvent(List<Stream> streamList) {
        this.streamList = streamList;
    }

    public List<Stream> getStreamList() {
        return this.streamList;
    }
}
