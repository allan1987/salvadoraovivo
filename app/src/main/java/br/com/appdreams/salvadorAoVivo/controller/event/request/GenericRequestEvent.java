package br.com.appdreams.salvadorAoVivo.controller.event.request;

import android.content.Context;

public abstract class GenericRequestEvent {

    protected final Context context;

    public GenericRequestEvent(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return this.context;
    }
}
