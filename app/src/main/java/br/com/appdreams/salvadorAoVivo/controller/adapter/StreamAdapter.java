package br.com.appdreams.salvadorAoVivo.controller.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.appdreams.salvadorAoVivo.model.Stream;
import br.com.appdreams.salvadorAoVivo.view.StreamItemView;
import br.com.appdreams.salvadorAoVivo.view.StreamItemView_;
import br.com.appdreams.salvadorAoVivo.view.ViewWrapper;

public class StreamAdapter extends RecyclerViewAdapterBase<Stream, StreamItemView> {

    public Context context;

    private List<Long> idList;

    public StreamAdapter(Context context) {
        this.context = context;
        this.idList = new ArrayList<>();
    }

    @Override
    protected StreamItemView onCreateItemView(ViewGroup parent, int viewType) {
        return StreamItemView_.build(this.context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<StreamItemView> viewHolder, int position) {
        StreamItemView view = viewHolder.getView();
        Stream stream = this.items.get(position);

        view.bind(stream);
    }

    public void addAll(List<Stream> streamList) {
        for(Stream stream : streamList) {
            if(!this.idList.contains(stream.getId())) {
                this.items.add(stream);
                this.idList.add(stream.getId());
            }
        }
    }
}