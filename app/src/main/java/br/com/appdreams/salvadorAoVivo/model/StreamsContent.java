package br.com.appdreams.salvadorAoVivo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties({""})
public class StreamsContent {

    @JsonProperty("streams")
    private List<Stream> streamList;

    public StreamsContent() {}

    public List<Stream> getStreamList() {
        return this.streamList;
    }
}
